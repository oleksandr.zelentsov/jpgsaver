#!/usr/bin/env python3
import os
import shutil
from tempfile import NamedTemporaryFile, mktemp
from zipfile import ZipFile
from argparse import ArgumentParser


def to_archive(jpg_filename, *files, destination=None, backup=False):
    if len(files) == 0:
        raise Exception('no files to save')
    elif not all([os.path.isfile(x) or os.path.isdir(x) for x in files]):
        raise Exception('not all files are able to be saved')

    # create tmp file location
    result_fn = mktemp(prefix='jsvr', suffix='.jpg')
    # copy jpg to that location
    shutil.copy(jpg_filename, result_fn)
    # create zip archive with all the files
    zipf = mktemp(prefix='jsvr', suffix='.zip')
    with ZipFile(zipf, mode='w') as zipfp:
        for file_ in files:
            zipfp.write(file_)
    # append the bytes from zip archive to the jpg file
    with open(result_fn, 'b+a') as fp:
        with open(zipf, mode='b+r') as zipfp:
            fp.write(zipfp.read())
    os.remove(zipf)

    if backup:
        shutil.move(jpg_filename, '{}.bak'.format(jpg_filename))
    if destination is None:
        shutil.move(result_fn, jpg_filename)
    else:
        shutil.move(result_fn, destination)


def get_args():
    parser = ArgumentParser(description='Save files into jpg images.')
    parser.add_argument('filename', help='filename of a fresh JPEG file')
    parser.add_argument('content', nargs='*', help='files to save inside an image')
    parser.add_argument('-d', '--destination', help='result filename')
    parser.add_argument('-b', '--backup', action='store_true', default=False, help='create a .jpg.bak file with original picture')
    return parser.parse_args()


if __name__ == '__main__':
    args = get_args()
    to_archive(args.filename, *(args.content), destination=args.destination, backup=args.backup)
